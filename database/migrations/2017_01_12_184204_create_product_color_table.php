<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductColorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_color', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('color_id');
            $table->unsignedInteger('product_id');
            $table->rememberToken();
            $table->timestamps();

            $table->unique(array('color_id', 'product_id'));
        });

        Schema::table('product_color', function (Blueprint $table) {
          $table->foreign('color_id')
            ->references('id')->on('colors')
            ->onDelete('cascade')
            ->onUpdate('cascade');
          $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_color', function (Blueprint $table) {
          $table->dropForeign('product_color_color_id_foreign');
          $table->dropForeign('product_color_product_id_foreign');
        });

        Schema::dropIfExists('product_color');
    }
}
