<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_collection', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('collection_id');
            $table->unsignedInteger('product_id');
            $table->rememberToken();
            $table->timestamps();
            $table->unique(array('collection_id', 'product_id'));
        });

        Schema::table('product_collection', function (Blueprint $table) {
          $table->foreign('collection_id')
            ->references('id')->on('collections')
            ->onDelete('cascade')
            ->onUpdate('cascade');
          $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_collection', function (Blueprint $table) {
          $table->dropForeign('product_collection_collection_id_foreign');
          $table->dropForeign('product_collection_product_id_foreign');
        });

        Schema::dropIfExists('product_collection');
    }
}
