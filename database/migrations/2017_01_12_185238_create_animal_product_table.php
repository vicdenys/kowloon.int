<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_animal', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('animal_id');
            $table->unsignedInteger('product_id');
            $table->unique(array('animal_id', 'product_id'));
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('product_animal', function (Blueprint $table) {
          $table->foreign('animal_id')
            ->references('id')->on('animals')
            ->onDelete('cascade')
            ->onUpdate('cascade');
          $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('product_animal', function (Blueprint $table) {
        $table->dropForeign('product_animal_animal_id_foreign');
        $table->dropForeign('product_animal_product_id_foreign');
      });

      Schema::dropIfExists('product_animal');
    }
}
