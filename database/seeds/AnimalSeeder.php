<?php

use Illuminate\Database\Seeder;

class AnimalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('animals')->delete();

      $animals = ['Dog', 'Cat', 'Fish', 'Bird', 'Small Animals'];

      foreach ($animals as $animal) {
          DB::table('animals')->insert([ //,
              'name' => $animal
          ]);
      }
    }
}
