<?php

use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('collections')->delete();

    $collections = ['Splash ‘n Fun', 'Luxury', 'new', 'on sale'];

    foreach ($collections as $collection) {
      DB::table('collections')->insert([ //,
        'name' => $collection
      ]);
    }
  }

}
