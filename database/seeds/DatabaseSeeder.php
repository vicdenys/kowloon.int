<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(QuestionsSeeder::class);
        $this->call(CollectionSeeder::class);
        $this->call(AnimalSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(ProductSeeder::class);
    }
}
