<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * @var
     */
    private $animalPivotData = [];
    private $collectionPivotData = [];
    private $colorPivotData = [];
    /**
     * @var
     */
    private $totalValues = 100;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->delete();

        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([ //,
                'name' => $faker->text($maxNbChars = 20),
                'description' => $faker->text($maxNbChars = 1000),
                'price' => rand(10000,1)/100,
                'clicks' => rand(10000,1)
            ]);
        }


        for ($x = 0; $x <= $this->totalValues; $x++) {
          //-- get a new pivot value
          $new_animal_pivot_value = self::newPivot('animal', $this->animalPivotData);
          $new_color_pivot_value = self::newPivot('color', $this->colorPivotData);
          $new_collection_pivot_value = self::newPivot('collection', $this->collectionPivotData);
          //-- lets add ot to the array for validation
          array_push($this->animalPivotData, $new_animal_pivot_value);
          array_push($this->colorPivotData, $new_color_pivot_value);
          array_push($this->collectionPivotData, $new_collection_pivot_value);
          //-- add pivot data to db
          DB::table('product_color')->insert([
              'color_id' => $new_color_pivot_value[0],
              'product_id' => $new_color_pivot_value[1]
          ]);
          DB::table('product_animal')->insert([
              'animal_id' => $new_animal_pivot_value[0],
              'product_id' => $new_animal_pivot_value[1]
          ]);
          DB::table('product_collection')->insert([
              'collection_id' => $new_collection_pivot_value[0],
              'product_id' => $new_collection_pivot_value[1]
          ]);
        }
    }

    private function newPivot($type, array $pivotData){
      if($type == 'animal') $pivot_value =  self::getAnimalData();
      if($type == 'color') $pivot_value =  self::getColorData();
      if($type == 'collection') $pivot_value =  self::getCollectionData();

      $result = self::in_array_r($pivot_value, $pivotData);
      //-- if its a duplicate then re-run method
      if($result) {
        if($type == 'animal') return self::newPivot('animal', $pivotData);
        if($type == 'color') return self::newPivot('color', $pivotData);
        if($type == 'collection') return self::newPivot('collection', $pivotData);
      }
      return $pivot_value;
    }

    private function getColorData(){
      $faker =  Faker\Factory::create();
      $colorId =  $faker->randomElement(App\Color::pluck('id')->toArray());
      $productId =  $faker->randomElement(App\Product::pluck('id')->toArray());
        return [
          (Int)$colorId,
          (Int)$productId
        ];
    }
    private function getAnimalData(){
      $faker =  Faker\Factory::create();
      $animal =  $faker->randomElement(App\Animal::pluck('id')->toArray());
      $productId =  $faker->randomElement(App\Product::pluck('id')->toArray());
        return [
          (Int)$animal,
          (Int)$productId
        ];
    }
    private function getCollectionData(){
      $faker =  Faker\Factory::create();
      $collection =  $faker->randomElement(App\Collection::pluck('id')->toArray());
      $productId =  $faker->randomElement(App\Product::pluck('id')->toArray());
        return [
          (Int)$collection,
          (Int)$productId
        ];
    }

    private function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item)
                && self::in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }
}
