<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('colors')->delete();

        $colors = ['White', 'Blue', 'Black', 'Red'];

        foreach ($colors as $color) {
          DB::table('colors')->insert([ //,
            'name' => $color
          ]);
        }
    }
}
