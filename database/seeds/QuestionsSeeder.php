<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questions')->delete();
      
      $faker = Faker\Factory::create();

      $limit = 33;

      for ($i = 0; $i < $limit; $i++) {
          DB::table('questions')->insert([ //,
              'question' => 'Is dit een vraag?',
              'answer' => $faker->realText($maxNbChars = 255, $indexSize = 2)
          ]);
      }
    }
}
