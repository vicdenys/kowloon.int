
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../../../node_modules/bootstrap-slider/dist/bootstrap-slider');


(function() {

  var $carousel = $('#carousel');
  var $progressBar = $('.progress-bar');
  var carouselInterval = 10;// in Seconds
  var firstLoad = true;

  //----------------------------------
  // Initialize Carousel
  //----------------------------------
  $carousel.carousel({
    interval: false
  });

  $(document).ready( function () {

    //----------------------------------
    // Start Timer and Carousel
    //----------------------------------
    var width = 0;
    var id = setInterval(frame, 10);
    function frame() {
      if (width >= 100) {
        width = 0;
        $carousel.carousel('next');
      } else {
        width += (1 / carouselInterval);
        $progressBar.width(width + '%');
      }
    }

    //----------------------------------
    // Add listeners to nav buttons
    //----------------------------------
    $('#searchBtn').on('click', function(){
        toggleModal($(this));
    });
    $('#faqBtn').on('click', function(){
        toggleModal($(this));
    });

    function toggleModal(object) {
      $('#modal1').modal('show');
      $('#bs-example-navbar-collapse-1').collapse('hide');

      $('#searchBtn').removeClass('active');
      $('#faqBtn').removeClass('active');
      object.addClass('active');

      $.ajax({
        type : 'GET',
        url : object.data('path'),

        success: function(result) {
          $('#modal1 div.modal-body').html(result);

          $('#answerWrapper').on('scroll', checkScroll);

        }
      });
    }

    //----------------------------------
    // Remove active class from links
    //----------------------------------
    $('#modal1').on('hidden.bs.modal', function () {
        $('#searchBtn').removeClass('active');
        $('#faqBtn').removeClass('active');
        $('#modal1 div.modal-body').html("");
    });

    //----------------------------------
    // Remove breack icon when focused on input
    //----------------------------------
    $("body").on('keypress', '#faq-input', function () {
        $("#break-icon").hide();
        $('#faqLoader').show();
        $('.nothing-found').remove();
    });

    $("body").on('keyup', '#faq-input', function () {
      $('.answer-wrapper').empty();
      if ($('#faq-input').val() != ""){
        getQuestions($('#faq-input').data('path'));
        $('#faqLoader').hide();
      }
    });

    $('.nav').find('.icon').click(function () {
      window.location.replace($(this).next().attr('href'));
    })

    $("body").on('click', "#clearFaq", function () {
      $("#faq-input").val("");
      $("#break-icon").show();
      $('.answer-wrapper').empty();
      $('#faqLoader').hide();
    });

    $("body").on('blur', '#faq-input', function () {
      if($(this).val() === ""){
        $("#break-icon").show();
        $('#faqLoader').hide();
      }
    });

    $('.question').click(function () {
      if($(this).find('.btnToggle').hasClass('open')){
        $(this).find('p').slideUp();
        $(this).find('.btnToggle').removeClass('open');
      }
      else{
        $(this).find('.btnToggle').addClass('open');
        $(this).find('p').slideDown();
      }
    });

    $('.gallery-link').click(function () {
      $('.images').find('.gallery-link.active').removeClass('active');
      $(this).addClass('active');
      $('#bigImg').attr('src', $(this).find('img').attr('src'));
    });

    $('.product-index').ready(function () {
      $('#title').text(getUrlParameter('animal') + ' articles.');

      var mySlider = $("#ex2").slider({});

      $("#ex2").on("change", function(slideEvt) {
      	$('.maxValue').val(slideEvt.value.newValue[1]);
        $('.minValue').val(slideEvt.value.newValue[0]);
      });

      $("#ex2").on("slideStop", function(slideEvt) {
        $('#gallery-wrapper').empty();
        getProducts($('.product-index').data('path'), slideEvt.value[0], slideEvt.value[1] );
      });

      $('.checkbox').find('.wrapper').click(function () {
        window.location.replace($(this).data('page'));
      })

      $('.filterToggle').click(function () {
        if($(this).find('.btnToggle').hasClass('open')){
          $('.filter-controls').slideUp();
          $(this).find('.btnToggle').removeClass('open');
        }
        else{
          $(this).find('.btnToggle').addClass('open');
          $('.filter-controls').slideDown();
        }
      });

      $('.maxValue').on('blur', function () {
        setslider();
      });
      $('.minValue').on('blur', function () {
        setslider();
      });

      function setslider() {
        var min = parseInt($('.minValue').val());
        var max = parseInt($('.maxValue').val());
        mySlider.slider('setValue', [ min, max ], false, false);
        $('#gallery-wrapper').empty();
        getProducts($('.product-index').data('path'), min, max);
      }


      getProducts($('.product-index').data('path'));

      $(document).scroll(function () {
        checkProductScroll();
      })
    });

    //----------------------------------
    // Search on scroll paginate
    //----------------------------------

    function checkScroll () {
      var page = $('.answer-wrapper').attr('data-next-page');

      if(page){
        clearTimeout($.data( this, "scrollCheck" ));

        $.data(this, "scrollCheck", setTimeout(function() {
          var scrollPos = $('#answerWrapper').height() + $('#answerWrapper').scrollTop() + 750;

          if (scrollPos >= $(document).height()){
            getQuestions($('.answer-wrapper').attr('data-next-page'));
          }
        }, 350));
      }
    }

    function checkProductScroll () {
      var page = $('#gallery-wrapper').attr('data-next-page');

      if(page){
        clearTimeout($.data( this, "scrollCheck" ));

        $.data(this, "scrollCheck", setTimeout(function() {
          var scrollPos = $('body').height() + $('body').scrollTop() + 200;

          if (scrollPos >= $(document).height()){
            getProducts(page);
          }
        }, 350));
      }
    }

    //----------------------------------
    // Load new questions
    //----------------------------------

    function getQuestions(url) {
      $('#faqLoader').show();
      $.ajax({
        type : 'POST',
        data : {
          keyword: $('#faq-input').val()
        },
        url : url,
        headers: {
          'X-CSRF-TOKEN': $("input[name='_token'").val()
        },

        success: function(result) {
          $('.answer-wrapper').append(result.questions);
          $('.answer-wrapper').attr('data-next-page', result.next_page_url);
          $('#faqLoader').hide();
          if(result.questions === ""){
              $('.nothing-found').remove();
              $( ".answer-wrapper" ).append( "<h4 class='nothing-found'>Niets gevonden.</4>" );
          }
        }
      });
    }

    function getProducts(url, minv, maxv) {

      var min = (minv ? minv : 0);
      var max = (maxv ? maxv : 0);

      $('#faqLoader').show();

      $.ajax({
        type : 'POST',
        data : {
          animal: getUrlParameter('animal'),
          min : min,
          max: max
        },
        url : url,
        headers: {
          'X-CSRF-TOKEN': $("input[name='_token'").val()
        },

        success: function(result) {
          $('#gallery-wrapper').append(result.products);
          $('#gallery-wrapper').attr('data-next-page', result.next_page_url);
          $('#faqLoader').hide();
          if(result.products === ""){
              $('.nothing-found').remove();
              $( "#gallery-wrapper" ).append( "<h4 class='nothing-found'>Niets gevonden.</4>" );
          }
        }
      });
    }

    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
      }
  };
});

})();
