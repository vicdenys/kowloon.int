@extends('layouts.master')

@section('title')
Products
@endsection


@section('content')

  @include('partials.header')

  <div class="container product-index" data-path='{{ url("/productajax")}}'>
    {{ csrf_field() }}
    <div class="breadcrumbs">
      <h1 class="logo">
        <a href="/">K</a>
      </h1>
      <ul>
          <li class="animal {{ strtolower($_GET['animal'])}}"><a href="">{{ $_GET['animal'] }}</a></li>
      </ul>
    </div>

    <div class="row">
      <div class="col-md-12">
        <h2 id="title"></h2>
      </div>
    </div>

    <div class="row filter-wrapper">
      <div class="col-md-12 filterToggle">
        <p>Filter</p>
        <div class="btnToggle"></div>
      </div>

      <div class="col-md-12 filter-controls">
        <div class="col-sm-10 col-sm-offset-1">
          <h4>Catagory</h4>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
          <div class="checkbox row">
            <div class="wrapper" data-page="{{ url('/product?animal=dog')}}">
              <input id="dog-r" type="radio" name="animal" value="" {{ ($_GET['animal'] == 'dog') ? 'checked' : ''  }}>
              <label for="dog-r">Dogs</label>
              <div class="check"></div>
            </div>
            <div class="wrapper" data-page="{{ url('/product?animal=cat')}}">
              <input id="cat-r"  type="radio" name="animal" value="" {{ ($_GET['animal'] === 'cat') ? 'checked' : ''  }}>
              <label for="cat-r">Cats</label>
              <div class="check"></div>
            </div>
            <div class="wrapper" data-page="{{ url('/product?animal=bird')}}">
              <input id="bird-r"  type="radio" name="animal" value="" {{ ($_GET['animal'] === 'bird') ? 'checked' : ''  }}>
              <label for="bird-r">Birds</label>
              <div class="check"></div>
            </div>
            <div class="wrapper" data-page="{{ url('/product?animal=fish')}}">
              <input id="fish-r"  type="radio" name="animal" value="" {{ ($_GET['animal'] === 'fish') ? 'checked' : ''  }}>
              <label for="fish-r">Fish</label>
              <div class="check"></div>
            </div>
            <div class="wrapper" data-page="{{ url('/product?animal=dog')}}">
              <input id="other-r"  type="radio" name="animal" value="" {{ ($_GET['animal'] === 'small') ? 'checked' : ''  }}>
              <label for="other-r">Other</label>
              <div class="check"></div>
            </div>

          </div>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
          <h4>Price Range</h4>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
          <input id="ex2" type="text" class="span2" value="" data-slider-min="0" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,1000]"/>
          <div class="min-max-wrapper">
            <input  class="minValue" type="text" name="min" value="0">
            -
            <input id="maxValue" class="maxValue" type="text" name="max" value="1000">
          </div>
        </div>
      </div>

      <div class="col-md-12 line-break"></div>
    </div>

    <div class="row product-wrapper">
      <div class="row hot-items">
        <div class="col-sm-12 gallery-wrapper">
          <div class="row" id="gallery-wrapper">

          </div>
          <div class="col-md-12 text-center">
            <i id='faqLoader' class="fa fa-spinner fa-spin" style="font-size:24px"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
