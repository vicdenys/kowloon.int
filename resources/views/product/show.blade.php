@extends('layouts.master')

@section('title')
{{ $product->name }}
@endsection


@section('content')

  <div class="container product-show">
    <div class="row">
      <div class="col-md-12 logo-wrapper">
        <img src="/img/logo-kowloon.png" alt="logo kowloon">
      </div>
    </div>

    <div class="row details-wrapper">

      <div class="breadcrumbs breadcrumbs-top">
        @include('partials.breadcrumbs')
      </div>
      <div class="col-md-6 mobile-header">
        <h2 class="title">
          {{ $product->name }}
        </h2>
        <h4 class="price">
          € {{ $product->price}}
        </h4>
      </div>
      <div class="col-md-6 images">
        <div class="row">
          <div class="col-xs-12">
            <div class="img-wrapper">
              <img id="bigImg" src="/img/hot-item-1.png" alt="dog cooling mat">
            </div>
          </div>
          <div class="col-xs-4 gallery-link active">
            <div class="img-wrapper">
              <img src="/img/hot-item-1.png" alt="dog cooling mat">
            </div>
            <p>klein tekstje</p>
          </div>
          <div class="col-xs-4 gallery-link">
            <div class="img-wrapper">
              <img src="/img/hot-item-1.png" alt="dog cooling mat">
            </div>
            <p>klein tekstje</p>
          </div>
          <div class="col-xs-4 gallery-link">
            <div class="img-wrapper">
              <img src="/img/hot-item-1.png" alt="dog cooling mat">
            </div>
            <p>klein tekstje</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 text">
        <div class="breadcrumbs breadcrumbs-2">
          @include('partials.breadcrumbs')
        </div>
        <h2 class="title">
          {{ $product->name }}
        </h2>
        <h4 class="price">
          € {{ $product->price}}
        </h4>
        @if(count($product->colors))
          <div class="colors">
            <h4>Colors</h4>
            @foreach ($product->colors as $color)
              <div class="color-{{ strtolower($color->name) }}"></div>
            @endforeach
          </div>
        @endif
        <div class="description">
          <h4>Description</h4>
          <p>{{ substr($product->description, 0, 400) }}</p>
        </div>
      </div>
    </div>

    <div class="row specs-wrapper">
      <div class="col-md-12">
        <h3>Specifications</h3>
        <h4>Dimensions</h4>
        <ul>
          <li>S - Ø 53x18cm</li>
          <li>M - Ø 53x18cm</li>
          <li>L - Ø 53x18cm</li>
        </ul>
        <h4>Titel</h4>
        <ul>
          <li>S - hier komt technische tekst</li>
        </ul>
      </div>
    </div>

    <div class="row hot-items">
      <div class="col-md-12">
        <h1 class="title">gerelateerde producten</h1>
      </div>
      <div class="col-sm-12 gallery-wrapper" id="gallery-wrapper">
        <div class="row">
          <div class="scroll-prev"><</div>
          @include('partials.gallery')
          <div class="scroll-next">></div>
        </div>
      </div>
      <div class="col-xs-12">
        <a href="#" class="visit-store">
          view more
        </a>
      </div>
    </div>

    <div class="row hot-items">
      <div class="col-md-12">
        <h1 class="title">Frequently Asked Questions</h1>
      </div>
      <div class="col-sm-12">
        <div class="row questions-wrapper">
          <div class="col-md-12 question">
            <div class="btnToggle"></div>
            <h3>Dit is een vraag</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
          </div>
          <div class="col-md-12 question">
            <div class="btnToggle"></div>
            <h3>Dit is een vraag</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
          </div>
          <div class="col-md-12 question">
            <div class="btnToggle"></div>
            <h3>Dit is een vraag</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
          </div>
        </div>
      </div>
      <div class="col-xs-12">
        <a href="#" class="visit-store">
          More Questions?
        </a>
      </div>
    </div>


    @include('partials.newsletter')
  </div>

@endsection
