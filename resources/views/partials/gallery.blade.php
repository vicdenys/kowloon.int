@foreach($otherProducts as $product)
  <div class="col-xs-3 item-wrapper {{ (rand(0, 1)) ? 'item-wrapper-multiple-images' :'' }}">
    <a href="{{ url('product', [$product->id]) }}">
      <div class="img-wrapper">
        @if(count($product->colors))
          <div class="colors">
            @foreach($product->colors as $color)
              <div class="color-{{ strtolower($color->name) }}"></div>
            @endforeach
          </div>
        @endif

        <div class="img-count">
          <p>{{ rand(2, 4) }}</p>
        </div>

        <img src="/img/hot-item-1.png" alt="dog cooling mat">

        <div class="link-overlay">
          <div class="icon information-icon" alt="infromation icon"></div>
        </div>
      </div>
      <p class="name">{{ $product->name }}</p>
      <p class="price">€ {{ $product->price }}</p>
    </a>
  </div>
@endforeach
