<nav class="navbar">
    <div class="navbar-header">
      <button type="button"  data-hide="modal"  class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav nav-top">
        <li id='searchBtn' data-path="{{ url('/search')}}" data-target="#modal1">
          <div class="icon search-icon" src="" alt="search-icon"></div>
          <a  href="#" >Search</a>
        </li>
        <li id='faqBtn' data-path="{{ url('/faq')}}" data-target="#modal1">
          <div class="icon faq-icon" src="" alt="faq-icon"></div>
          <a href="#">FAQ</a>
        </li>
      </ul>

      <ul class="nav navbar-nav nav-cat">
        <li class="{{  (isset($_GET['animal']) && $_GET['animal'] === 'dog') ? 'active' : '' }}">
          <div class="icon dog-icon " src="" alt="dog-icon"></div>
          <a href="{{ url('/product?animal=dog')}}">Dogs</a>
        </li>
        <li class="{{(isset($_GET['animal']) &&  $_GET['animal'] === 'cat') ? 'active' : '' }}">
          <div class="icon cat-icon " src="" alt="cat-icon"></div>
          <a href="{{ url('/product?animal=cat')}}">Cats</a>
        </li>
        <li class="{{(isset($_GET['animal']) &&  $_GET['animal'] === 'fish') ? 'active' : '' }}">
          <div class="icon fish-icon " src="" alt="fish-icon"></div>
          <a href="{{ url('/product?animal=fish')}}">Fish</a>
        </li>
        <li class="{{(isset($_GET['animal']) &&  $_GET['animal'] === 'bird') ? 'active' : '' }}">
          <div class="icon bird-icon " src="" alt="birds-icon"></div>
          <a href="{{ url('/product?animal=bird')}}">Birds</a>
        </li>
        <li class="{{(isset($_GET['animal']) &&  $_GET['animal'] === 'small-animals') ? 'active' : '' }}">
          <div class="icon small-animals-icon " src="" alt="small-animals-icon"></div>
          <a href="#">Small animals</a>
        </li>
      </ul>

    </div>

    <h1 class="logo">
      <a href="/">K<span>owloon</span></a>
    </h1>


</nav>
