<div class="page-header">

  <div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carousel" data-slide-to="0" class="active">
        <div class="indicator-inner"></div>
      </li>
      <li data-target="#carousel" data-slide-to="1">
        <div class="indicator-inner"></div>
      </li>
      <li data-target="#carousel" data-slide-to="2">
        <div class="indicator-inner"></div>
      </li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img class="d-block img-fluid" src="img/dog_header_pink.jpg" alt="dog with pink overlay">
      </div>
      <div class="item">
        <img class="d-block img-fluid" src="img/dog_header_blue.jpg" alt="dog with blue overlay">
      </div>
      <div class="item">
        <img class="d-block img-fluid" src="img/dog_header_yellow.jpg" alt="dog with yellow overlay">
      </div>
    </div>
  </div>

  <div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
  </div>

  <img class='logo' src="img/logo-kowloon.png" alt="Logo from kowloon">


</div>
