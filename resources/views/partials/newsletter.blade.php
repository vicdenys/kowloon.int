<div class="row newsletter">
  <div class="col-sm-8 newsletter-banner">
    <h2>
      discover amazing
      <br/>
      Kowloon deals!
    </h2>
    <p>only in our newsletter</p>
  </div>
  <div class="col-sm-4 newsletter-form-wrapper">
    <h3>Subscribe to our newsletter</h3>
    <p>Lorum ipsum dolor sit amet..</p>
    <div class="row">
      <form action="">
        <div class="form-group">
          <div class="col-xs-12">
            <input type="email" required class="form-control" id="inputPassword3" placeholder="Domain @ name.com">
            <button type="submit" class="btn btn-default">Ok</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
