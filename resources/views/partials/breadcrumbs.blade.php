<h1 class="logo">
  <a href="/">K</a>
</h1>
<ul>
  @foreach($product->animals as $animal)
    <li class="animal {{ strtolower($animal->name)}}"><a href="">{{ $animal->name }}</a></li>
  @endforeach
  @foreach($product->collections as $collection)
    <li> <a href="">{{ $collection->name }}</a></li>
  @endforeach
</ul>
