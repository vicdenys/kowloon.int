<div class="faq-wrapper container">
  <div class="row">
    <div class="col-md-12">
      <h1 class="title"><span>FAQ</span><span>Frequently Asked Questions</span></h1>
    </div>
  </div>

  <div class="row">

      <div class="form-group search-field">
        <div class="col-xs-12">
          <div class="icon search-icon"></div>
          {{ csrf_field() }}
          <input type="text" required class="form-control" id="faq-input" data-path="{{ url('/faqsearch')}}" placeholder="Search on keyword">
          <div  id="break-icon"  class="icon break-icon"></div>
        </div>
        <div class="col-xs-12 clear-btn-wrapper">
          <button id="clearFaq">&#9746; Clear</button>
        </div>
      </div>
  </div>

  <div class="row customers-wrapper">
    <div class="col-md-12">
      <p>Don’t find what you’re looking for?</p>
      <p>You can always contact our <a href="#">customer service.</a></p>
      <p>We’re happy to help you!</p>
    </div>
  </div>

  <div class="row answer-wrapper"  id="answerWrapper" data-next-page="">

  </div>
  <div class="row">
    <div class="col-md-12 text-center">
      <i id='faqLoader' class="fa fa-spinner fa-spin" style="font-size:24px"></i>
    </div>
  </div>
</div>
