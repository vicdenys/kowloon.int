<div class="search-wrapper container">
  <div class="row">

    <div class="form-group">
      <div class="col-sm-8 col-sm-offset-2">
        <h4>Catagory</h4>
      </div>
      <div class="col-sm-8 col-sm-offset-2">
        <div class="checkbox row">
          <label><input type="checkbox" value="">Dogs</label>
          <label><input type="checkbox" value="">Cats</label>
          <label><input type="checkbox" value="">Fish</label>
          <label><input type="checkbox" value="">Birds</label>
          <label><input type="checkbox" value="">Other</label>
        </div>
      </div>
    </div>

    <div class="form-group search-field">
      <div class="col-xs-12">
        <div class="icon search-icon"></div>
        {{ csrf_field() }}
        <input type="text" required class="form-control" id="faq-input" data-path="{{ url('/faqsearch')}}" placeholder="Just start typing and hit">
        <div  id="break-icon"  class="icon break-icon"></div>
      </div>
      <div class="col-xs-12 clear-btn-wrapper">
        <button id="clearFaq">&#9746; Clear</button>
      </div>
    </div>
  </div>

  <div class="row customers-wrapper">
    <div class="col-md-12">
      <p>Don’t find what you’re looking for? Maybe use fewer words or a more general search term.If you still have no luck you can contact our <a href="#">customer service.</a></p>
    </div>
  </div>
</div>
