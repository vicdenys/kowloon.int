@extends('layouts.master')

@section('title')
Home
@endsection


@section('content')

  @include('partials.header')

  <div class="container home">
    <div class="row">
      <div class="col-md-12">
        <p class="description">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <span>
          Duis aute irure dolor in reprehenderit in.
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
        </p>
      </div>
    </div>

    <div class="row catagories">
      <div class="col-md-2">
        <a href="{{url('/product?animal=dog')}}">
          <div class="icon dog-icon" src="" alt="dog-icon"></div>
          <p>Dogs</p>
        </a>
      </div>
      <div class="col-md-2">
        <a href=" {{ url('/product?animal=cat')}}">
          <div class="icon cat-icon" src="" alt="cat-icon"></div>
          <p>Cats</p>
        </a>
      </div>
      <div class="col-md-2">
        <a href="{{url('/product?animal=fish')}}">
          <div class="icon fish-icon" src="" alt="fish-icon"></div>
          <p>Fish</p>
        </a>
      </div>
      <div class="col-md-2">
        <a href="{{url('/product?animal=bird')}}">
          <div class="icon bird-icon" src="" alt="bird-icon"></div>
          <p>Birds</p>
        </a>
      </div>
      <div class="col-md-2">
        <a href="{{url('/product?animal=dog')}}">
          <div class="icon small-animals-icon" src="" alt="small-animals-icon"></div>
          <p>Small Animals</p>
        </a>
      </div>
      <div class="col-md-2">
        <a href="{{url('/product?animal=dog')}}">
          <h4>+</h4>
          <p>Other</p>
        </a>
      </div>
    </div>

    <div class="row hot-items">
      <div class="col-md-12">
        <h1 class="title">Hot Items.</h1>
      </div>
      <div class="col-sm-12 gallery-wrapper">
        <div class="row">
          @include('partials.gallery')
        </div>
      </div>
      <div class="col-xs-12">
        <a href="#" class="visit-store">
          Visit the store
        </a>
      </div>
    </div>

    @include('partials.newsletter')
  </div>

@endsection
