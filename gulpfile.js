const elixir = require('laravel-elixir'),
      sprity = require('sprity'),
      gulpif = require('gulp-if');
      productCount = 100;

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */



elixir.extend('spriteImg', function() {
    new elixir.Task('sprites', function () {
      return sprity.src({
          src: 'resources/assets/img/**/*.{png,jpg}',
          style: './_sprite.scss',
          cssPath: './../img',
            // ... other optional options
            // for example if you want to generate scss instead of css
          processor: 'scss', // make sure you have installed sprity-sass
        })
        .pipe(gulpif('*.png', gulp.dest('public/img/'), gulp.dest('resources/assets/sass/sprites')));
    });
});


elixir((mix) => {
    mix.spriteImg();
    mix.sass('app.scss')
       .webpack('app.js')
       .version(['css/app.css', 'js/app.js']);
});
