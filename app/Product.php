<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public function colors()
    {
        return $this->belongsToMany('App\Color', 'product_color');
    }

    //
    public function collections()
    {
        return $this->belongsToMany('App\Collection', 'product_collection');
    }
    public function animals()
    {
        return $this->belongsToMany('App\Animal', 'product_animal');
    }
}
