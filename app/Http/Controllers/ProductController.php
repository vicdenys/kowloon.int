<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    //

    public function index()
    {
      return view('product.index');
    }

    public function indexAjax(Request $r)
    {
      if(!$r->ajax()){
        abort(404, 'Page not found');
      }

      if($r->min || $r->max){
        $otherProducts = Product::join('product_animal', 'products.id', '=', 'product_animal.product_id')
              ->join('animals', 'product_animal.animal_id', '=', 'animals.id')
              ->select('animals.id', 'animals.name', 'products.price', 'products.id', 'products.name', 'products.description')
              ->where('animals.name' , '=', $r->animal)
              ->where('products.price' , '>=', $r->min)
              ->where('products.price' , '<=', $r->max)
              ->paginate();
      }else{
        $otherProducts = Product::join('product_animal', 'products.id', '=', 'product_animal.product_id')
              ->join('animals', 'product_animal.animal_id', '=', 'animals.id')
              ->select('animals.id', 'animals.name', 'products.price', 'products.id', 'products.name', 'products.description')
              ->where('animals.name' , '=', $r->animal)
              ->paginate();
      }


      return [
        'products' => view('partials.gallery', compact('otherProducts'))->render(),
        'productsjson' => $otherProducts,
        'next_page_url' => $otherProducts->nextPageUrl()
      ];
    }

    public function show(Product $product)
    {
      $otherProducts = collect(new Product);
      if(count($product->collections)){
        foreach($product->collections as $collection){
          $otherProducts = $otherProducts->merge($collection->products()->take(10)->get());
        }
        $otherProducts = $otherProducts->chunk(10)[0];
      }


      $product->clicks ++;
      $product->save();
      $product->colors = $product->colors()->get();
      $product->animals = $product->animals()->get();
      $product->collections = $product->collections()->get();

      return view('product.show', compact('product', 'otherProducts'));
    }
}
