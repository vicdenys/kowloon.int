<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Product;

class HomeController extends Controller
{
    //

    public function index(){
      $otherProducts = Product::orderBy('clicks', 'desc')->take(4)->get();

      foreach($otherProducts as $product){
        $product->colors = $product->colors()->get();
      }

      return view('home', compact('otherProducts'));
    }

    public function faqPage(Request $r){
      if($r->ajax()){
        return view('faq');
      }

      abort(404, 'Page not found');
    }

    public function searchPage(Request $r){
      if($r->ajax()){
        return view('search');
      }

      abort(404, 'Page not found');
    }

    public function faqSearch(Request $r){
      if(!$r->ajax()){
        abort(404, 'Page not found');
      }

      $keyword = $r->keyword;

      $questions = Question::where('question', 'like', "%$keyword%")
              ->orWhere('answer', 'like', "%$keyword%")->paginate(4);

      return [
        'questions' => view('partials.question', compact('questions'))->render(),
        'next_page_url' => $questions->nextPageUrl()
      ];
    }
}
